const {createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //Vetor de imagens locais
            imagensLocais: [
                './Imagens/lua.jpg',
                './Imagens/sol.jpg',
                './Imagens/SENAI_logo.png'
            ],

            imagensInternet: [
                'https://i.ytimg.com/vi/Y2_KNa6gecs/maxresdefault.jpg',
                'https://images.hgmsites.net/lrg/2018-dodge-challenger-srt-demon-carbon-fiber-edition-by-speedkore--photo-credit-bj-motors_100736641_l.jpg',
                'https://uploads.diariodopoder.com.br/2022/09/f28ebfeb-412178_1028802_ricardo_7_20220831_642_uk2drive_rodrigoruiz_04779-960x640.jpg'

            ],
        };// Fim return
    }, // Fim data
    computed:{
        randomImage(){
            return this.imagensLocais[this.randomIndex];
        },//Fim randomImage
        randomImageInternet(){
            return this.imagensInternet[this.randomIndexInternet]
        }//Fim randomImageInternet
    },//Fim computed

    methods:{
        getRandomImage()
        {            
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);
            this.randomIndexInternet = Math.floor(Math.random()*this.imagensInternet.length);
        }
    },//Fim methods
}).mount("#app");
