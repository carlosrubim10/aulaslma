const {createApp}= Vue;
    createApp({
        data() {
            return{
                testeSpan: false,
                isLampadaLigada: false,
            }
        }, //Fim data
        methods:{
            handleTest: function()
        {
            this.testeSpan = !this.testeSpan;
        }, //Fim handletest
            toggleLampada: function()
            {
                this.isLampadaLigada = !this.isLampadaLigada;
            }
        
    } // Fim methods

    }).mount(`#app`)